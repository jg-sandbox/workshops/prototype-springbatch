package com.juliusgb.workshops.pocs.springbatch.jdbc;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.juliusgb.workshops.pocs.springbatch.jdbc.conf.AppJdbcConfig;
import com.juliusgb.workshops.pocs.springbatch.jdbc.conf.CustomBatchConfigurer;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.Customer;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.CustomerRowMapper;

@Configuration
@Import({AppJdbcConfig.class, CustomBatchConfigurer.class})
@EnableBatchProcessing
public class JdbcCursorJob {
	
	// custom classes
	@Autowired
	private JobRepository jobRepo;
	
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	
	@Bean
	public JdbcCursorItemReader<Customer> customerItemReader(@Autowired @Qualifier("appDatasource") DataSource dataSource) {
		return new JdbcCursorItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.dataSource(dataSource)
				.sql("select * from customer where city=\'Hamilton\'")
				.rowMapper(new CustomerRowMapper())
				.build();
	}
	
	@Bean
	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}
	
	@Bean
	public Step stepJdbcCursor() {
		return this.stepBuilderFactory.get("stepJdbcCursor")
				.<Customer, Customer>chunk(10)
				.reader(customerItemReader(null))
				.writer(itemWriter())
				.build();
	}
	
	@Bean
	public Job jbdcCursorJob() {
		return this.jobBuilderFactory.get("jbdcCursorJob").repository(jobRepo)
				.start(stepJdbcCursor())
				.build();
	}

}
