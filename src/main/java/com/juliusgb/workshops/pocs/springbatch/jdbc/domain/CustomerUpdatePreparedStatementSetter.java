package com.juliusgb.workshops.pocs.springbatch.jdbc.domain;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.batch.item.database.ItemPreparedStatementSetter;

public class CustomerUpdatePreparedStatementSetter implements ItemPreparedStatementSetter<Customer> {
	private static final String FILTER_COLUMN_TEXT = "Ready to Test1";
	
	@Override
	public void setValues(Customer customer, PreparedStatement ps) throws SQLException {
		
		// SET
		ps.setString(1, FILTER_COLUMN_TEXT);
	}
	

}
