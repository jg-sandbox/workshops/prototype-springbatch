package com.juliusgb.workshops.pocs.springbatch.jdbc.conf;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class AppJdbcConfig {
	
	@Bean(name = "appDatasource")
	@Primary
    public DataSource myAppDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/test_db?currentSchema=public");
        dataSource.setUsername("test_user");
        dataSource.setPassword("test_pass");
 
        return dataSource;
    }

	@Bean(name="appTransactionManager")
	public DataSourceTransactionManager dataSourceTransationManager(@Autowired @Qualifier("appDatasource") DataSource dataSource) {
		final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(dataSource);
		return transactionManager;
	}

	@Bean(name="appJdbcTemplate")
	public JdbcTemplate jdbcTemplate(@Autowired @Qualifier("appDatasource") DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		return jdbcTemplate;
	}
}
