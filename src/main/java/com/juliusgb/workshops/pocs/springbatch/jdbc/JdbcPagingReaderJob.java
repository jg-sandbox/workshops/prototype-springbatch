package com.juliusgb.workshops.pocs.springbatch.jdbc;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.juliusgb.workshops.pocs.springbatch.jdbc.conf.AppJdbcConfig;
import com.juliusgb.workshops.pocs.springbatch.jdbc.conf.CustomBatchConfigurer;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.Customer;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.CustomerRowMapper;

@Configuration
@Import({AppJdbcConfig.class, CustomBatchConfigurer.class})
@EnableBatchProcessing
public class JdbcPagingReaderJob {
	
	// custom classes
	@Autowired
	private JobRepository jobRepo;
	
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	
	@Bean
	@StepScope
	public JdbcPagingItemReader<Customer> customerItemReader(
			@Autowired @Qualifier("appDatasource") DataSource dataSource,
			PagingQueryProvider queryProvider,
			@Value("#{jobParameters['city']}") String city) {
		
		Map<String, Object> parameterValues = new HashMap<>(1);
		parameterValues.put("city", city);
		
		return new JdbcPagingItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.dataSource(dataSource)
				.queryProvider(queryProvider)
				.parameterValues(parameterValues)
				.pageSize(10)
				.rowMapper(new CustomerRowMapper())
				.build();
		
	}
	
	@Bean
	public SqlPagingQueryProviderFactoryBean pagingQueryProvider(@Autowired @Qualifier("appDatasource") DataSource dataSource) {
		SqlPagingQueryProviderFactoryBean factoryBean = new SqlPagingQueryProviderFactoryBean();
		// useful to workout which database is being used
		factoryBean.setDataSource(dataSource);
		factoryBean.setSelectClause("select *");
		factoryBean.setFromClause("customer");
		factoryBean.setWhereClause("where city = :city");
		// important so that record order is guaranteed across query executions
		// has to also be unique - springbatch uses it when it's creating the sql query to execute
		factoryBean.setSortKey("last_name"); 
		
		return factoryBean;
	}
	
	@Bean
	public ItemWriter<Customer> itemWriter() {
		return (items) -> items.forEach(System.out::println);
	}
	
	@Bean
	public Step stepJdbcPage1() {
		return this.stepBuilderFactory.get("stepJdbcPage1")
				.<Customer, Customer>chunk(10)
				.reader(customerItemReader(null, null, null))
				.writer(itemWriter())
				.build();
	}
	
	@Bean
	public Job jdbcPagingRead() {
		return this.jobBuilderFactory.get("jdbcPagingRead")
				.repository(jobRepo)
				.start(stepJdbcPage1())
				.build();
	}

}
