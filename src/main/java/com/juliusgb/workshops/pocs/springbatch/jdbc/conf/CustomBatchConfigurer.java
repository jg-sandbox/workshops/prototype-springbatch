package com.juliusgb.workshops.pocs.springbatch.jdbc.conf;

import javax.sql.DataSource;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.explore.support.JobExplorerFactoryBean;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Configures the JobRepository and JobExplorer, since both use the 
 * underlying datasource and should be in sync.
 */
@Configuration
@Import(SpringBatchMetadataJdbcConfig.class)
public class CustomBatchConfigurer extends DefaultBatchConfigurer {
	private static final String DATABASE_TYPE="POSTGRES";

	@Autowired
	@Qualifier("springBatchMetadataDatasource")
	private DataSource dataSource;
	
	@Autowired
	@Qualifier("springBatchTransactionManager")
	private PlatformTransactionManager transactionManager;
	

	@Override
	protected JobRepository createJobRepository() throws Exception {
		JobRepositoryFactoryBean factoryBean = new JobRepositoryFactoryBean();
		factoryBean.setDatabaseType(DATABASE_TYPE);
		factoryBean.setIsolationLevelForCreate("ISOLATION_REPEATABLE_READ");
		factoryBean.setDataSource(this.dataSource);
		factoryBean.setTransactionManager(transactionManager);
		// would've been called directly by spring container if we didn't override the
		// createJobRepository method
		factoryBean.afterPropertiesSet();
		return factoryBean.getObject();
	}

	@Override
	public PlatformTransactionManager getTransactionManager() {		
		return this.transactionManager;
	}
	
	@Bean(name="springBatchJdbcTemplate")
	public JdbcTemplate jdbcTemplate(@Autowired @Qualifier("springBatchMetadataDatasource") DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		return jdbcTemplate;
	}

	// read-only view of the batch metadata
	@Override
	protected JobExplorer createJobExplorer() throws Exception {
		JobExplorerFactoryBean factoryBean = new JobExplorerFactoryBean();
		factoryBean.setDataSource(this.dataSource);
		factoryBean.afterPropertiesSet();
		return factoryBean.getObject();
	}
}
