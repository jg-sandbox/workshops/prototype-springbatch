package com.juliusgb.workshops.pocs.springbatch.tasklets;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.MethodInvokingTaskletAdapter;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class HelloTaskletCallingAnotherService {
	
	@Autowired
	private JobRepository jobRepo;

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Bean
	public Job callServiceJob() {
		return this.jobBuilderFactory.get("callServiceJob")
				.repository(jobRepo)
				.start(step1()).build();
	}

	@Bean
	public Step step1() {
		return this.stepBuilderFactory.get("step1")
				.tasklet(methodInvokingTasklet(null)).build();
	}
	
	@StepScope
	@Bean
	public MethodInvokingTaskletAdapter methodInvokingTasklet(@Value("#{jobParameters['message']}") String message) {
		MethodInvokingTaskletAdapter methodInvokingTaskletAdapter = new MethodInvokingTaskletAdapter();
		methodInvokingTaskletAdapter.setTargetObject(helloServiceObj());
		methodInvokingTaskletAdapter.setTargetMethod("helloService");
		methodInvokingTaskletAdapter.setArguments(new String[] { message });
		return methodInvokingTaskletAdapter;
	}
	
	@Bean
	public HelloService2 helloServiceObj() {
		return new HelloService2();
	}
}

class HelloService2 {
	public void helloService(String message) {
		System.out.println("Message war: " + message);
	}
}
