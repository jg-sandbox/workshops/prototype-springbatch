package com.juliusgb.workshops.pocs.springbatch.jdbc.domain;

import lombok.Data;

@Data
public class Customer {

	private Long id;
	private String firstName;
	private String middleInitial;
	private String lastName;
	private String addressNumber;
	private String street;
	private String address;
	private String city;
	private String state;
	private String zipCode;


}
