package com.juliusgb.workshops.pocs.springbatch.jdbc.restart.stop;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.listener.JobListenerFactoryBean;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.juliusgb.workshops.pocs.springbatch.jdbc.conf.AppJdbcConfig;
import com.juliusgb.workshops.pocs.springbatch.jdbc.conf.CustomBatchConfigurer;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.Customer;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.CustomerRowMapper;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.CustomerUpdatePreparedStatementSetter;
import com.juliusgb.workshops.pocs.springbatch.jdbc.listener.JobLoggingListener;
import com.juliusgb.workshops.pocs.springbatch.jdbc.listener.StepLoggingListener;

@Configuration
@Import({ AppJdbcConfig.class, CustomBatchConfigurer.class })
@EnableBatchProcessing
public class JdbcPagingStopping2 {
	private static final int PAGE_SIZE_AS_COMMIT_INTERVAL = 25;

	// custom classes
	@Autowired
	private JobRepository jobRepo;

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Bean
	@StepScope
	public CustomerItemReaderForStopping2 customerReaderStopper4() {
		return new CustomerItemReaderForStopping2(customerJdbcPagingItemReader4(null,null));
	}

	@Bean
	@StepScope
	public JdbcPagingItemReader<Customer> customerJdbcPagingItemReader4(
			@Autowired @Qualifier("appDatasource") DataSource dataSource, PagingQueryProvider queryProvider) {

		return new JdbcPagingItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.dataSource(dataSource)
				.queryProvider(queryProvider)
				.pageSize(PAGE_SIZE_AS_COMMIT_INTERVAL)
				.rowMapper(new CustomerRowMapper())
				.build();
	}

	@Bean
	public SqlPagingQueryProviderFactoryBean pagingQueryProvider4(@Autowired @Qualifier("appDatasource") DataSource dataSource) {
		
		SqlPagingQueryProviderFactoryBean factoryBean = new SqlPagingQueryProviderFactoryBean();

		factoryBean.setDataSource(dataSource);
		factoryBean.setSelectClause("select *");
		factoryBean.setFromClause("customer");
		factoryBean.setSortKey("last_name");

		return factoryBean;
	}

	@Bean
	public ItemWriter<Customer> jdbcBatchItemWriter4(@Autowired @Qualifier("appDatasource") DataSource dataSource) {

		String updateSql = "UPDATE CUSTOMER SET test_column1 = ?";

		return new JdbcBatchItemWriterBuilder<Customer>()
				.dataSource(dataSource).sql(updateSql)
				.itemPreparedStatementSetter(new CustomerUpdatePreparedStatementSetter4())				
				.build();
	}

	@Bean
	public Step stepForStopping4() {
		return this.stepBuilderFactory.get("stepForStopping2")
				.<Customer, Customer>chunk(PAGE_SIZE_AS_COMMIT_INTERVAL)
				.reader(customerReaderStopper4())
				.writer(jdbcBatchItemWriter4(null))
				.allowStartIfComplete(true)
				.listener(customerReaderStopper4())				
				.build();
	}
	
//	@Bean
//	public Flow flowStep2() {
//		return new FlowBuilder<Flow>("flowStep")
//				.start(stepForStopping2())
//				.on("STOPPED").stopAndRestart(stepForStopping2())
//				.build();
//	}

	@Bean
	public Job jdbcPagingStoppingJob4() {
		return this.jobBuilderFactory.get("jdbcPagingStoppingJob4").repository(jobRepo)
				.start(stepForStopping4())
				.listener(JobListenerFactoryBean.getListener(new JobLoggingListener()))
				.build();
	}

}

