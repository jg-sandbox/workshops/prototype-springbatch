package com.juliusgb.workshops.pocs.springbatch.jdbc.listener;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.listener.JobListenerFactoryBean;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.juliusgb.workshops.pocs.springbatch.jdbc.conf.AppJdbcConfig;
import com.juliusgb.workshops.pocs.springbatch.jdbc.conf.CustomBatchConfigurer;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.Customer;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.CustomerRowMapper;
import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.CustomerUpdatePreparedStatementSetter;

@Configuration
@Import({ AppJdbcConfig.class, CustomBatchConfigurer.class })
@EnableBatchProcessing
public class JdbcPagingJobWithListeners {
	private static final int PAGE_SIZE_AS_COMMIT_INTERVAL = 25;

	// custom classes
	@Autowired
	private JobRepository jobRepo;

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Bean
	@StepScope
	public JdbcPagingItemReader<Customer> customerItemReader(
			@Autowired @Qualifier("appDatasource") DataSource dataSource, PagingQueryProvider queryProvider,
			@Value("#{jobParameters['city']}") String city) {

		Map<String, Object> parameterValues = new HashMap<>(1);
		parameterValues.put("city", city);

		return new JdbcPagingItemReaderBuilder<Customer>()
				.name("customerItemReader")
				.dataSource(dataSource)
				.queryProvider(queryProvider)
				.parameterValues(parameterValues)
				.pageSize(PAGE_SIZE_AS_COMMIT_INTERVAL)
				.rowMapper(new CustomerRowMapper())
				.build();
	}

	@Bean
	public SqlPagingQueryProviderFactoryBean pagingQueryProvider(
			@Autowired @Qualifier("appDatasource") DataSource dataSource) {
		SqlPagingQueryProviderFactoryBean factoryBean = new SqlPagingQueryProviderFactoryBean();
		// useful to workout which database is being used
		factoryBean.setDataSource(dataSource);
		factoryBean.setSelectClause("select *");
		factoryBean.setFromClause("customer");
		//factoryBean.setWhereClause("where city = :city");
		// important so that record order is guaranteed across query executions
		// has to also be unique - springbatch uses it when it's creating the sql query
		// to execute
		factoryBean.setSortKey("last_name");
		//factoryBean.setSortKeys(sortKeys);

		return factoryBean;
	}

	@Bean
	public ItemWriter<Customer> jdbcBatchItemWriter(@Autowired @Qualifier("appDatasource") DataSource dataSource) {

		String updateSql = "UPDATE CUSTOMER SET test_column1=?";

		return new JdbcBatchItemWriterBuilder<Customer>()
				.dataSource(dataSource).sql(updateSql)
				.itemPreparedStatementSetter(new CustomerUpdatePreparedStatementSetter())				
				.build();
	}

	@Bean
	public Step stepJdbcPagingReaderWriter() {
		return this.stepBuilderFactory.get("stepJdbcPagingReaderWriter")
				.<Customer, Customer>chunk(PAGE_SIZE_AS_COMMIT_INTERVAL)
				.reader(customerItemReader(null, null, null))
				.writer(jdbcBatchItemWriter(null))
				.listener(new StepLoggingListener())
				.build();
	}

	@Bean
	public Job jdbcPagingWithListeners() {
		return this.jobBuilderFactory.get("jdbcPagingWithListeners").repository(jobRepo)
				.start(stepJdbcPagingReaderWriter())
				.listener(JobListenerFactoryBean.getListener(new JobLoggingListener()))
				.build();
	}

}

