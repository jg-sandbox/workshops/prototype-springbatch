# poc-springbatch

Prototype for Springbatch jobs.

Requirements: 

* Docker (to run PostgreSQL)

* Maven to build the project and update PostreSQL with Liquibase.

* pgAdmin (https://www.pgadmin.org/) to view the database. This can also be done in Eclipse.


# Prep and prerequisites

## Build springboot uber jar

mvn clean install

Run jobs from command line

```
java -jar target/springbatch-0.0.1-SNAPSHOT.jar <fully-qualified-class-name> <job-method-name> <other-params>
```

where fully qualified classname containing the job definitions

* chunkJob is the method name or bean name of the job
* the rest are parameters passed to the job itself

## Run Liquibase changes to create the tables in Postgres

- mvn package
- mvn -P liquibase-docker liquibase:update


# Spring Batch

There are a few jobs here. All use JDBC. Of interest are those based on the JdbcCursor JdbcPaging implementations.

## Available Jobs

### Job with JDBC Cursor Item Reader

<p> A Job using a JdbcCursor that reads from database using hard-coded sql. The writer prints to system the items found.</p>

<p>To run it, execute<br/> 
`TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.JdbcCursorJob jbdcCursorJob city=Hamilton timestamp=$TIMESTAMP`

</p>

<p> A Job using a JdbcCursor that reads from database using parameters passed in when the job runs. The writer prints to system the items found</p>

<p>To run it, execute<br/> 
`TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.JbdcCursorJobWithoutHardcodedSql jbdcCursorJobWithoutHardcodedSql city=Hamilton timestamp=$TIMESTAMP`
</p>

### Job using JDBC Paging for the Item Reader. 

<p> A Job using a JdbcPager that reads from database using parameters passed in when the job runs. The writer prints to system the items found</p>

<p>To run it, execute <br/> 
`TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.JdbcPagingReaderJob jdbcPagingRead city=Hamilton timestamp=$TIMESTAMP`
</p>

### Job using JDBC Paging for the ItemReader and ItemWriter. 

<p>Job that uses a JdbcPagingItemReader (reads all records) and a JdbcPagingItemWriter (updates a single column). It's chunk size = page size.</p>

<p>To run it and to keep the log that we can examine later, execute <br/>
`TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.JdbcPagingReaderWithJdbcPagingJob jdbcPagingReaderWriter city=Hamilton timestamp=$TIMESTAMP > out7.log`
</p>

<p>Copy the log's contents into Notepad++</p>

- Search for 'Reading page '. It should appear 20 times. There's 500 records in the database. If the pagesize=25, then we'd expect 20 pages.

- Search for 'Repeat is complete according to policy and result value.' It should appear about 25 times.</p>

### Job using JDBC Paging plus Listeners. 


<p>To run it and to keep the log that we can examine later, execute <br/>
`TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.listener.JdbcPagingJobWithListeners jdbcPagingWithListeners city=Hamilton timestamp=$TIMESTAMP > out8.log`
</p>

### Misc

<p>To run it and to keep the log that we can examine later, execute <br/>
`TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.tasklets.HelloTaskletCallingAnotherService callServiceJob message=hellao timestamp=$TIMESTAMP > out13.log`
</p>


### Jobs with 2 options to manually STOP a batch Job

#### Variante 1 - In afterStep() überprüfen

<p>Überprüfungen wurde nicht bestanden</p>

* CustomerItemReaderForStopping anpassen mit
<br/>
Linie 22 einblenden/aktivieren. Linie 23 auskommentieren.

* mvn clean install

* job ausführen
<br/>
TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.restart.stop.JdbcPagingStopping jdbcPagingStoppingJob timestamp=$TIMESTAMP > out026.log

* log (out026.log) prüfen: nach 'Check failed' suchen

* datenbank prüfen (src/main/db/springbatch/prototypes.sql): status=STOPPED und exit_code=STOPPED

	
<p>Anpassung, damit den Lauf nicht fehlschlägt</p>

* CustomerItemReaderForStopping anpassen mit
<br/>
Linie 65 einblenden/aktivieren. Linie 63 auskommentieren.

* mvn clean install

* job ausführen
<br/>
TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.restart.stop.JdbcPagingStopping jdbcPagingStoppingJob timestamp=2020-09-30.21:42:36.931 > out027.log

* log (out027.log) prüfen: nach 'Check failed' suchen

* datenbank prüfen: status=STOPPED und exit_code=STOPPED 
	
#### Variante 2	- In read() überprüfen

<p>Überprüfungen wurde nicht bestanden</p>

* CustomerItemReaderForStopping2 anpassen mit
<br/>
Linie 22 einblenden/aktivieren. Linie 23 auskommentieren.

* mvn clean install

* job ausführen
<br/>
TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.restart.stop.JdbcPagingStopping2 jdbcPagingStoppingJob2 timestamp=$TIMESTAMP > out030.log

* log (out030.log) prüfen: nach 'Check failed' suchen

* datenbank prüfen: status=STOPPED und exit_code=STOPPED
	
<p>Anpassung, damit den Lauf nicht fehlschlägt</p>

* CustomerItemReaderForStopping2 anpassen mit
<br/>
Linie 65 einblenden/aktivieren. Linie 63 auskommentieren.

* mvn clean install

* job ausführen
<br/>
TIMESTAMP=`date +%Y-%m-%d.%H:%M:%S.%3N` && java -jar target/springbatch-0.0.1-SNAPSHOT.jar com.juliusgb.workshops.pocs.springbatch.jdbc.restart.stop.JdbcPagingStopping2 jdbcPagingStoppingJob2 timestamp=2020-10-01.07:10:43.090 > out027.log

* log (out027.log) prüfen: nach 'Check failed' suchen

* datenbank prüfen: status=COMPLETED und exit_code=COMPLETED