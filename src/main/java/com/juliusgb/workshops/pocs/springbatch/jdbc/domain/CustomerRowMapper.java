package com.juliusgb.workshops.pocs.springbatch.jdbc.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CustomerRowMapper implements RowMapper<Customer> {
	
	@Override
	public Customer mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		Customer customer = new Customer();

		customer.setId(resultSet.getLong("customer_id")); // TODO: should be customer_id
		customer.setAddress(resultSet.getString("address1")); // was address but should be address1
		customer.setCity(resultSet.getString("city"));
		customer.setFirstName(resultSet.getString("first_name")); // corrected
		customer.setLastName(resultSet.getString("last_name")); // corrected
		customer.setMiddleInitial(resultSet.getString("middle_name")); // corrected
		customer.setState(resultSet.getString("state"));
		customer.setZipCode(resultSet.getString("postal_code")); // corrected

		return customer;
	}

}
