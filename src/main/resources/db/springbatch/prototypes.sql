SELECT * FROM customer
ORDER BY last_name ASC;

update customer set test_column1='Ready To Test0';

UPDATE customer SET test_column1='Not Ready for Test' 
WHERE customer_id IN (
	SELECT customer_id 
	FROM customer 
	WHERE test_column1='Ready To Test0' 
	ORDER BY last_name ASC LIMIT 25);
	
SELECT COUNT(customer_id) FROM customer WHERE test_column1='Ready To Test0';
SELECT COUNT(customer_id) FROM customer WHERE test_column1='Ready to Test1';


select 
	bji.job_instance_id, bji.job_name, bjp.key_name, 
	bjp.string_val, 
	bje.job_execution_id,
	bje.status as batch_job_exec_status, bje.exit_code as bje_exitcode, bje.exit_message,
	bse.step_execution_id,
	bse.step_name,
	bse.exit_code as step_exitcode,
	bse.exit_message as step_message,
	bse.commit_count, bse.rollback_count,
	bse.read_count, bse.read_skip_count, 
	bse.write_count, bse.write_skip_count
from 
	springbatch.batch_job_instance bji
inner join
	springbatch.batch_job_execution bje on bji.job_instance_id = bje.job_instance_id
inner join
	springbatch.batch_job_execution_params bjp on bjp.job_execution_id = bje.job_execution_id
inner join
	springbatch.batch_step_execution bse on bse.job_execution_id = bje.job_execution_id
order by 
	bji.job_instance_id desc,
	bje.job_execution_id desc,
	bse.step_execution_id desc;