package com.juliusgb.workshops.pocs.springbatch.jdbc.restart.stop;

import java.util.Objects;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.JdbcPagingItemReader;

import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.Customer;

public class CustomerItemReaderForStopping2 implements ItemStreamReader<Customer> {
	
	private ItemStreamReader<Customer> customerReader; 
	private int itemsRead = 0; 
	//private int expectedItemsToGoFromSTOPPEDTOCOMPLETED = 498;
	private int expectedItemsTriggerSTOPPED = 10;
	
	private StepExecution stepExecution;
	
	public CustomerItemReaderForStopping2(JdbcPagingItemReader<Customer> customerReader) {
		this.customerReader = customerReader;
	}
	
	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		customerReader.open(executionContext);
		
	}

	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		customerReader.update(executionContext);
		
	}

	@Override
	public void close() throws ItemStreamException {
		customerReader.close();
		
	}

	@Override
	public Customer read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		Customer c = customerReader.read();
		
		if (Objects.nonNull(c)) {
			itemsRead++;
		}	
		else {
			
			if (itemsRead != expectedItemsTriggerSTOPPED) {
			//if (itemsRead >= expectedItemsToGoFromSTOPPEDTOCOMPLETED) {
				System.out.println("Check FAILED");
				String message = "ItemsRead=%s; expectedItemsToProcess=%s" ;
				System.out.println(String.format(message, itemsRead, expectedItemsTriggerSTOPPED));
				
				this.stepExecution.setTerminateOnly();
			}
		}
		
		return c;
	}

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

}
