package com.juliusgb.workshops.pocs.springbatch.jdbc.restart.stop;

import java.util.Objects;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.JdbcPagingItemReader;

import com.juliusgb.workshops.pocs.springbatch.jdbc.domain.Customer;

public class CustomerItemReaderForStopping implements ItemStreamReader<Customer>  {
	
	private ItemStreamReader<Customer> customerReader; 
	private int itemsRead = 0; 
	//private int expectedItemsToGoFromSTOPPEDTOCOMPLETED = 498;
	private int expectedItemsTriggerSTOPPED = 10;
	
	public CustomerItemReaderForStopping(JdbcPagingItemReader<Customer> customerReader) {
		this.customerReader = customerReader;
	}
	
	
	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		customerReader.open(executionContext);
		
	}

	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		customerReader.update(executionContext);
		
	}

	@Override
	public void close() throws ItemStreamException {
		customerReader.close();
		
	}

	@Override
	public Customer read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		Customer c = customerReader.read();
		
		if (Objects.nonNull(c)) {
			itemsRead++;
		}
		
		return c;
	}
	
	@AfterStep
	public ExitStatus afterStep(StepExecution stepExecution) {
		System.out.println("In AfterStep");
		
		if (itemsRead != expectedItemsTriggerSTOPPED) {
		
		//if (itemsRead >= expectedItemsToGoFromSTOPPEDTOCOMPLETED) {
			System.out.println("Check PASSED");
			System.out.println("itemsRead=" + itemsRead + "; Setting STATUS to " + stepExecution.getExitStatus());
			
			return stepExecution.getExitStatus();
		}
		else {
			System.out.println("Check FAILED");
			String message = "ItemsRead=%s; expectedItemsToProcess=%s" ;
			System.out.println(String.format(message, itemsRead, expectedItemsTriggerSTOPPED));

			
			return ExitStatus.STOPPED;
		}
	}

}
