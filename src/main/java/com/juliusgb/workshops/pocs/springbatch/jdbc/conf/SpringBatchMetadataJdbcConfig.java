package com.juliusgb.workshops.pocs.springbatch.jdbc.conf;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class SpringBatchMetadataJdbcConfig {

	@Bean(name = "springBatchMetadataDatasource")
    public DataSource mySpringbatchMetadataDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/test_db?currentSchema=springbatch");
        dataSource.setUsername("test_user");
        dataSource.setPassword("test_pass");
 
        return dataSource;
    }
	
	@Bean(name="springBatchTransactionManager")
	public  PlatformTransactionManager getBatchTransactionManager(@Autowired @Qualifier("springBatchMetadataDatasource") DataSource dataSource) {
		final DataSourceTransactionManager batchTransactionManager = new DataSourceTransactionManager();
		batchTransactionManager.setDataSource(dataSource);
		return batchTransactionManager;
	}
}
