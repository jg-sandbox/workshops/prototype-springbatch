package com.juliusgb.workshops.pocs.springbatch.jdbc.listener;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;

public class StepLoggingListener implements StepExecutionListener {
	

	//@BeforeStep
	@Override
	public void beforeStep(StepExecution stepExecution) {
		String startMessage = "%s has begun!";
		String stepName = stepExecution.getStepName();

		System.out.println(String.format(startMessage, stepName));
		
		stepExecution.setTerminateOnly();
	}
	

	//@AfterStep 
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) { 
		String startMessage = "%s has ended! und dauert %s";
		String stepName = null;
		
		if (Objects.nonNull(stepExecution)) {
			stepName = stepExecution.getStepName();
		}
		
		Instant jobStartTime = stepExecution.getStartTime().toInstant();
		Instant jobEndTime = stepExecution.getEndTime().toInstant();
		Duration duration = Duration.between(jobStartTime, jobEndTime); 

		System.out.println(String.format(startMessage, stepName, duration));
		
		return stepExecution.getExitStatus();
	}
}


