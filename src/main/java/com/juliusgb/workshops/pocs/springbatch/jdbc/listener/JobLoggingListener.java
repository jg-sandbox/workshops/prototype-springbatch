package com.juliusgb.workshops.pocs.springbatch.jdbc.listener;

import java.time.Duration;
import java.time.Instant;

import org.springframework.batch.core.JobExecution;

import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;

public class JobLoggingListener {
	
	@BeforeJob
	public void beforeJob(JobExecution jobExecution) {
		String startMessage = "%s is beginning execution at %s";
		String jobName = jobExecution.getJobInstance().getJobName();
		Instant jobStartTime = jobExecution.getStartTime().toInstant();
		
		System.out.println(String.format(startMessage, jobName, jobStartTime));
	}
	
	@AfterJob
	public void afterJob(JobExecution jobExecution) {
		String endMessage = "%s has completed with the status %s und dauert %s";
		String jobName = jobExecution.getJobInstance().getJobName();
		String jobStatus = jobExecution.getStatus().toString();
		Instant jobStartTime = jobExecution.getStartTime().toInstant();
		Instant jobEndTime = jobExecution.getEndTime().toInstant();
		Duration duration = Duration.between(jobStartTime, jobEndTime); 
		
		System.out.println(String.format(endMessage, jobName, jobStatus, duration));
	}

}
